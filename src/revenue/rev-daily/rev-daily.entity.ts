export class RevDailyTable {
  tanggal: string;
  m_1: string;
  m1: string;
  m: string;
  gapMom: string;
}

export class RevDailyChart {
  tanggal: string;
  m_1: number;
  m1: number;
  m: number;
  keteranganM_1: string;
  keteranganM1: string;
  keteranganM: string;
}

export class RevDailyChartResponse {
  tanggal: string[];
  m_1: number[];
  m1: number[];
  m: number[];
  keteranganM_1: string[];
  keteranganM1: string[];
  keteranganM: string[];
}

export class RevBranchChartResponse {
  mtd_dt: string;
  balikpapan: number;
  banjarmasin: number;
  palangkaraya: number;
  pontianak: number;
  samarinda: number;
  tarakan: number;
}
