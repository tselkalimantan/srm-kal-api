import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { RevDailyService } from './rev-daily.service';

describe('RevDailyService', () => {
  let service: RevDailyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers: [RevDailyService],
    }).compile();

    service = module.get<RevDailyService>(RevDailyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
