type RevDailyQuery = {
  teritori?: string;
  opt?: string;
  cat?: string;
  tahun?: number;
  bulan?: number;
  produk?: string;
  branch?: string;
  cluster?: string;
  city?: string;
  l1?: string;
  l2?: string;
  l3?: string;
};
