import { Injectable } from '@nestjs/common';
import { EntityManager, getManager } from 'typeorm';
import { parse, addDays, format, subMonths } from 'date-fns';
import { RevDailyChart, RevDailyChartResponse } from './rev-daily.entity';

@Injectable()
export class RevDailyService {
  entityManager: EntityManager;

  constructor() {
    this.entityManager = getManager('conn222');
  }

  async getRevDailyTable(query: RevDailyQuery) {
    const date = addDays(
      parse(`${query.tahun}-${query.bulan}-01`, 'yyyy-MM-dd', new Date()),
      1,
    );
    const bulan = format(date, 'MM');
    const bulanM1 = format(subMonths(date, 1), 'MM');
    const Y_1 = format(subMonths(date, 1), 'yyyy');
    const tahunLalu = query.tahun - 1;

    const where: RevDailyQuery = {};

    where.produk = `AND brand != 'kartuHALO'`;
    where.branch = '';
    where.cluster = '';
    where.city = '';
    where.l1 = '';
    where.l2 = '';
    where.l3 = '';
    where.cat = '';

    const whereRegion = "region_sales = 'KALIMANTAN'";
    if (query.produk) where.produk = `AND brand = '${query.produk}'`;
    if (query.branch) where.branch = `AND branch = '${query.branch}'`;
    if (query.cluster) where.cluster = `AND cluster_sales = '${query.cluster}'`;
    if (query.city) where.city = `AND kabupaten = '${query.city}'`;
    if (query.l1) where.l1 = `AND l1_name = '${query.l1}'`;
    if (query.l2) where.l2 = `AND l2_name = '${query.l2}'`;
    if (query.l3) where.l3 = `AND l3_name = '${query.l3}'`;
    if (query.cat) where.cat = `AND cat = '${query.cat}'`;

    const data = await this.entityManager.query(`
    SELECT
      tabTanggal.mtd_dt as tanggal,
      FORMAT(tabM_1.${query.opt}, 0) as m_1,
      FORMAT(tabM1.${query.opt}, 0) as m1,
      FORMAT(tabM.${query.opt}, 0) as m,
      FORMAT(tabM.${query.opt} - tabM1.${query.opt}, 0) as gapMom

      FROM

      (SELECT
      DATE_FORMAT(mtd_dt, '%d') as mtd_dt
      FROM
      rev_kal_2020
      WHERE month(mtd_dt) = 1
      GROUP BY
      rev_kal_2020.mtd_dt) as tabTanggal

      LEFT JOIN

      (SELECT
      DATE_FORMAT(mtd_dt, '%d') as mtd_dt,
      SUM(${query.opt}) as ${query.opt}
      FROM
      rev_kal_${tahunLalu}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      GROUP BY
      mtd_dt) as tabM_1

      ON tabTanggal.mtd_dt = tabM_1.mtd_dt

      LEFT JOIN

      (SELECT
      DATE_FORMAT(mtd_dt, '%d') as mtd_dt,
      SUM(${query.opt}) as ${query.opt}
      FROM
      rev_kal_${Y_1}
      WHERE
      MONTH(mtd_dt) = '${bulanM1}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      GROUP BY
      mtd_dt) as tabM1

      ON tabTanggal.mtd_dt = tabM1.mtd_dt

      LEFT JOIN

      (SELECT
      DATE_FORMAT(mtd_dt, '%d') as mtd_dt,
      SUM(${query.opt}) as ${query.opt}
      FROM
      rev_kal_${query.tahun}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      GROUP BY
      mtd_dt) as tabM

      ON tabTanggal.mtd_dt = tabM.mtd_dt

      UNION

      SELECT
      sum1.mtd_dt,
      FORMAT(sum1.${query.opt}, 0) as m_1,
      FORMAT(sum2.${query.opt}, 0) as m1,
      FORMAT(sum3.${query.opt}, 0) as m,
      CASE
        WHEN LAST_DAY('$date') = '$date'
        THEN FORMAT(IFNULL(sum3.${query.opt} - sum2.${query.opt}, 0),0)
        ELSE FORMAT(IFNULL(sum3.${query.opt} - sum2.${query.opt}_l, 0),0)
      END as gapMom
      FROM

      (SELECT
      'Sum' as mtd_dt,
      SUM(${query.opt}) as ${query.opt},
      (SELECT SUM(${query.opt})
      FROM
      rev_kal_${tahunLalu}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      DAY(mtd_dt) <= (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${query.tahun} WHERE MONTH(mtd_dt) = '${bulan}') AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as ${query.opt}_l
      FROM
      rev_kal_${tahunLalu}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as sum1

      JOIN

      (SELECT
      'Sum' as mtd_dt,
      SUM(${query.opt}) as ${query.opt},
      (SELECT SUM(${query.opt})
      FROM
      rev_kal_${Y_1}
      WHERE
      MONTH(mtd_dt) = '${bulanM1}' AND
      DAY(mtd_dt) <= (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${query.tahun} WHERE MONTH(mtd_dt) = '${bulan}') AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as ${query.opt}_l
      FROM
      rev_kal_${Y_1}
      WHERE
      MONTH(mtd_dt) = '${bulanM1}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as sum2

      JOIN

      (SELECT
      'Sum' as mtd_dt,
      SUM(${query.opt}) as ${query.opt},
      (SELECT SUM(${query.opt})
      FROM
      rev_kal_${query.tahun}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      DAY(mtd_dt) <= (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${query.tahun} WHERE MONTH(mtd_dt) = '${bulan}') AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as ${query.opt}_l
      FROM
      rev_kal_${query.tahun}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as sum3

      UNION
      
      SELECT
      avg1.mtd_dt,
      FORMAT((avg1.${query.opt} / avg1.totalDay), 0) as m_1,
      FORMAT((avg2.${query.opt} / avg2.totalDay), 0) as m1,
      FORMAT((avg3.${query.opt} / avg3.totalDay), 0) as m,
      CASE
      WHEN LAST_DAY('$date') = '$date'
        THEN FORMAT((avg3.${query.opt} - avg2_l.${query.opt}) / avg3.totalDay, 0)
        ELSE FORMAT((avg3.${query.opt} - avg2_l.${query.opt}) / ((avg3.totalDay + avg2.totalDay) / 2), 0)
      END as gapMom
      FROM
      
      (SELECT
      'Avg' as mtd_dt,
      SUM(${query.opt}) as ${query.opt},
      (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${tahunLalu} WHERE MONTH(mtd_dt) = '${bulan}') as totalDay
      FROM
      rev_kal_${tahunLalu}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as avg1
      
      JOIN
      
      (SELECT
      'Avg' as mtd_dt,
      SUM(${query.opt}) as ${query.opt},
      (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${Y_1} WHERE MONTH(mtd_dt) = '${bulanM1}') as totalDay
      FROM
      rev_kal_${Y_1}
      WHERE
      MONTH(mtd_dt) = '${bulanM1}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as avg2

      JOIN
      
      (SELECT
      'Avg' as mtd_dt,
      SUM(${query.opt}) as ${query.opt},
      (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${Y_1} WHERE MONTH(mtd_dt) = '${bulanM1}') as totalDay
      FROM
      rev_kal_${Y_1}
      WHERE
      MONTH(mtd_dt) = '${bulanM1}' AND
      DAY(mtd_dt) <= (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${query.tahun} WHERE MONTH(mtd_dt) = '${bulan}') AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as avg2_l
      
      JOIN
      
      (SELECT
      'Avg' as mtd_dt,
      SUM(${query.opt}) as ${query.opt},
      (SELECT DAY(MAX(mtd_dt)) FROM rev_kal_${query.tahun} WHERE MONTH(mtd_dt) = '${bulan}') as totalDay
      FROM
      rev_kal_${query.tahun}
      WHERE
      MONTH(mtd_dt) = '${bulan}' AND
      ${whereRegion}
      ${where.produk}
      ${where.branch}
      ${where.cluster}
      ${where.city}
      ${where.l1}
      ${where.l2}
      ${where.l3}
      ${where.cat}
      ) as avg3
    `);
    return data;
  }

  async getChart(query: RevDailyQuery) {
    const date = addDays(
      parse(`${query.tahun}-${query.bulan}-01`, 'yyyy-MM-dd', new Date()),
      1,
    );

    const bulanM1 = format(subMonths(date, 1), 'MM');
    const Y_1 = format(subMonths(date, 1), 'yyyy');
    const tahunLalu = query.tahun - 1;

    const where: RevDailyQuery = {};

    where.produk = `AND brand != 'kartuHALO'`;
    where.branch = '';
    where.cluster = '';
    where.city = '';
    where.l1 = '';
    where.l2 = '';
    where.l3 = '';
    where.cat = '';

    const whereRegion = "AND region_sales = 'KALIMANTAN'";
    if (query.produk) where.produk = `AND brand = '${query.produk}'`;
    if (query.branch) where.branch = `AND branch = '${query.branch}'`;
    if (query.cluster) where.cluster = `AND cluster_sales = '${query.cluster}'`;
    if (query.city) where.city = `AND kabupaten = '${query.city}'`;
    if (query.l1) where.l1 = `AND l1_name = '${query.l1}'`;
    if (query.l2) where.l2 = `AND l2_name = '${query.l2}'`;
    if (query.l3) where.l3 = `AND l3_name = '${query.l3}'`;
    if (query.cat) where.cat = `AND cat = '${query.cat}'`;

    const data: RevDailyChart[] = await this.entityManager.query(`
    SELECT
        tabTanggal.mtd_dt AS tanggal,
        ROUND(tabM_1.${query.opt}, 0) AS m_1,
        ROUND(tabM1.${query.opt}, 0) AS m1,
        ROUND(tabM.${query.opt}, 0) AS m,
        NULL AS keteranganM_1,
        NULL AS keteranganM1,
        NULL AS keteranganM
    FROM
        (
            SELECT
                DATE_FORMAT(rev_kal_2020.mtd_dt, '%d') AS mtd_dt
            FROM
                rev_kal_2020
            WHERE
                MONTH(mtd_dt) = 1
            GROUP BY
                rev_kal_2020.mtd_dt
        ) AS tabTanggal
        LEFT JOIN (
            SELECT
                DATE_FORMAT(mtd_dt, '%d') AS mtd_dt,
                SUM(${query.opt}) AS ${query.opt}
            FROM
                rev_kal_${tahunLalu}
            WHERE
                MONTH(mtd_dt) = '${query.bulan}'
                ${whereRegion}
                ${where.produk}
                ${where.branch}
                ${where.cluster}
                ${where.city}
                ${where.l1}
                ${where.l2}
                ${where.l3}
                ${where.cat}
            GROUP BY
                mtd_dt
        ) AS tabM_1 ON tabTanggal.mtd_dt = tabM_1.mtd_dt
        LEFT JOIN (
            SELECT
                DATE_FORMAT(mtd_dt, '%d') AS mtd_dt,
                SUM(${query.opt}) AS ${query.opt}
            FROM
                rev_kal_${Y_1}
            WHERE
                MONTH(mtd_dt) = '${bulanM1}'
                ${whereRegion}
                ${where.produk}
                ${where.branch}
                ${where.cluster}
                ${where.city}
                ${where.l1}
                ${where.l2}
                ${where.l3}
                ${where.cat}
            GROUP BY
                mtd_dt
        ) AS tabM1 ON tabTanggal.mtd_dt = tabM1.mtd_dt
        LEFT JOIN (
            SELECT
                DATE_FORMAT(mtd_dt, '%d') AS mtd_dt,
                SUM(${query.opt}) AS ${query.opt}
            FROM
                rev_kal_${query.tahun}
            WHERE
                MONTH(mtd_dt) = '${query.bulan}'
                ${whereRegion}
                ${where.produk}
                ${where.branch}
                ${where.cluster}
                ${where.city}
                ${where.l1}
                ${where.l2}
                ${where.l3}
                ${where.cat}
            GROUP BY
                mtd_dt
        ) AS tabM ON tabTanggal.mtd_dt = tabM.mtd_dt
    ORDER BY
        tabTanggal.mtd_dt
    `);

    const chartData: RevDailyChartResponse = {
      tanggal: [],
      m_1: [],
      m1: [],
      m: [],
      keteranganM_1: [],
      keteranganM1: [],
      keteranganM: [],
    };
    for (const object of data) {
      (chartData.tanggal as string[]).push(object.tanggal as string);
      (chartData.m_1 as number[]).push(object.m_1 as number);
      (chartData.m1 as number[]).push(object.m1 as number);
      (chartData.m as number[]).push(object.m as number);
      (chartData.keteranganM_1 as string[]).push(
        object.keteranganM_1 as string,
      );
      (chartData.keteranganM1 as string[]).push(object.keteranganM1 as string);
      (chartData.keteranganM as string[]).push(object.keteranganM as string);
    }

    return chartData;
  }

  getBranchClusterTable(query: RevDailyQuery) {
    const where: RevDailyQuery = {};

    where.produk = `AND brand != 'kartuHALO'`;
    where.l1 = '';
    where.cat = '';

    if (query.produk) where.produk = `AND brand = '${query.produk}'`;
    if (query.l1) where.l1 = `AND l1_name = '${query.l1}'`;
    if (query.cat) where.cat = `AND cat = '${query.cat}'`;

    let teritoriAs = '';
    if (query.teritori == 'cluster') {
      query.teritori = 'cluster_sales';
      teritoriAs = 'AS cluster';
    }

    const data = this.entityManager.query(`
    SELECT
        ${query.teritori} ${teritoriAs},
        FORMAT(SUM(IF(DAY(mtd_dt) = '01', ${query.opt}, 0)), 0) AS '01',
        FORMAT(SUM(IF(DAY(mtd_dt) = '02', ${query.opt}, 0)), 0) AS '02',
        FORMAT(SUM(IF(DAY(mtd_dt) = '03', ${query.opt}, 0)), 0) AS '03',
        FORMAT(SUM(IF(DAY(mtd_dt) = '04', ${query.opt}, 0)), 0) AS '04',
        FORMAT(SUM(IF(DAY(mtd_dt) = '05', ${query.opt}, 0)), 0) AS '05',
        FORMAT(SUM(IF(DAY(mtd_dt) = '06', ${query.opt}, 0)), 0) AS '06',
        FORMAT(SUM(IF(DAY(mtd_dt) = '07', ${query.opt}, 0)), 0) AS '07',
        FORMAT(SUM(IF(DAY(mtd_dt) = '08', ${query.opt}, 0)), 0) AS '08',
        FORMAT(SUM(IF(DAY(mtd_dt) = '09', ${query.opt}, 0)), 0) AS '09',
        FORMAT(SUM(IF(DAY(mtd_dt) = '10', ${query.opt}, 0)), 0) AS '10',
        FORMAT(SUM(IF(DAY(mtd_dt) = '11', ${query.opt}, 0)), 0) AS '11',
        FORMAT(SUM(IF(DAY(mtd_dt) = '12', ${query.opt}, 0)), 0) AS '12',
        FORMAT(SUM(IF(DAY(mtd_dt) = '13', ${query.opt}, 0)), 0) AS '13',
        FORMAT(SUM(IF(DAY(mtd_dt) = '14', ${query.opt}, 0)), 0) AS '14',
        FORMAT(SUM(IF(DAY(mtd_dt) = '15', ${query.opt}, 0)), 0) AS '15',
        FORMAT(SUM(IF(DAY(mtd_dt) = '16', ${query.opt}, 0)), 0) AS '16',
        FORMAT(SUM(IF(DAY(mtd_dt) = '17', ${query.opt}, 0)), 0) AS '17',
        FORMAT(SUM(IF(DAY(mtd_dt) = '18', ${query.opt}, 0)), 0) AS '18',
        FORMAT(SUM(IF(DAY(mtd_dt) = '19', ${query.opt}, 0)), 0) AS '19',
        FORMAT(SUM(IF(DAY(mtd_dt) = '20', ${query.opt}, 0)), 0) AS '20',
        FORMAT(SUM(IF(DAY(mtd_dt) = '21', ${query.opt}, 0)), 0) AS '21',
        FORMAT(SUM(IF(DAY(mtd_dt) = '22', ${query.opt}, 0)), 0) AS '22',
        FORMAT(SUM(IF(DAY(mtd_dt) = '23', ${query.opt}, 0)), 0) AS '23',
        FORMAT(SUM(IF(DAY(mtd_dt) = '24', ${query.opt}, 0)), 0) AS '24',
        FORMAT(SUM(IF(DAY(mtd_dt) = '25', ${query.opt}, 0)), 0) AS '25',
        FORMAT(SUM(IF(DAY(mtd_dt) = '26', ${query.opt}, 0)), 0) AS '26',
        FORMAT(SUM(IF(DAY(mtd_dt) = '27', ${query.opt}, 0)), 0) AS '27',
        FORMAT(SUM(IF(DAY(mtd_dt) = '28', ${query.opt}, 0)), 0) AS '28',
        FORMAT(SUM(IF(DAY(mtd_dt) = '29', ${query.opt}, 0)), 0) AS '29',
        FORMAT(SUM(IF(DAY(mtd_dt) = '30', ${query.opt}, 0)), 0) AS '30',
        FORMAT(SUM(IF(DAY(mtd_dt) = '31', ${query.opt}, 0)), 0) AS '31',
        FORMAT(SUM(${query.opt}), 0) AS 'ttl_rev'
    FROM
        rev_kal_${query.tahun}
    WHERE
        MONTH(mtd_dt) = '${query.bulan}'
        ${where.produk}
        ${where.l1}
        ${where.cat}
    GROUP BY
        ${query.teritori}
      `);

    return data;
  }

  async getBranchChart(query: RevDailyQuery) {
    const where: RevDailyQuery = {};

    where.produk = `AND brand != 'kartuHALO'`;
    where.l1 = '';
    where.l2 = '';
    where.l3 = '';
    where.cat = '';

    const data = this.entityManager.query(`
    SELECT
        DATE_FORMAT(mtd_dt, '%d') AS mtd_dt,
        SUM(IF(branch = 'BALIKPAPAN', rev, 0)) AS balikpapan,
        SUM(IF(branch = 'BANJARMASIN', rev, 0)) AS banjarmasin,
        SUM(IF(branch = 'PALANGKARAYA', rev, 0)) AS palangkaraya,
        SUM(IF(branch = 'PONTIANAK', rev, 0)) AS pontianak,
        SUM(IF(branch = 'SAMARINDA', rev, 0)) AS samarinda,
        SUM(IF(branch = 'TARAKAN', rev, 0)) AS tarakan
    FROM
        rev_kal_${query.tahun}
    WHERE
        MONTH(mtd_dt) = '${query.bulan}'
        ${where.produk}
        ${where.l1}
        ${where.l2}
        ${where.l3}
        ${where.cat}
    GROUP BY
        mtd_dt
    `);

    return data;
  }
}
