import { Controller, Get, Req } from '@nestjs/common';
import { ApiTags, ApiQuery, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import {
  RevBranchChartResponse,
  RevDailyChartResponse,
  RevDailyTable,
} from './rev-daily.entity';
import { RevDailyService } from './rev-daily.service';

@ApiTags('revenue')
@Controller('revenue/rev-daily')
export class RevDailyController {
  constructor(private readonly revDailyService: RevDailyService) {}

  @Get('table')
  @ApiOperation({ summary: 'Menampilkan tabel Revenue Daily' })
  @ApiQuery({ name: 'tahun', type: 'number' })
  @ApiQuery({ name: 'bulan', type: 'number' })
  @ApiQuery({ name: 'opt', type: 'string', example: 'rev' })
  @ApiQuery({ name: 'cat', type: 'string', required: false })
  @ApiQuery({ name: 'produk', type: 'string', required: false })
  @ApiQuery({ name: 'branch', type: 'string', required: false })
  @ApiQuery({ name: 'cluster', type: 'string', required: false })
  @ApiQuery({ name: 'city', type: 'string', required: false })
  @ApiQuery({ name: 'l1', type: 'string', required: false })
  @ApiQuery({ name: 'l2', type: 'string', required: false })
  @ApiQuery({ name: 'l3', type: 'string', required: false })
  getRevDailyTable(@Req() request: Request): Promise<RevDailyTable[]> {
    return this.revDailyService.getRevDailyTable(
      request.query as RevDailyQuery,
    );
  }

  @Get('chart')
  @ApiOperation({ summary: 'Menampilkan Chart Daily' })
  @ApiQuery({ name: 'tahun', type: 'number' })
  @ApiQuery({ name: 'bulan', type: 'number' })
  @ApiQuery({ name: 'opt', type: 'string', example: 'rev' })
  @ApiQuery({ name: 'cat', type: 'string', required: false })
  @ApiQuery({ name: 'produk', type: 'string', required: false })
  @ApiQuery({ name: 'branch', type: 'string', required: false })
  @ApiQuery({ name: 'cluster', type: 'string', required: false })
  @ApiQuery({ name: 'city', type: 'string', required: false })
  @ApiQuery({ name: 'l1', type: 'string', required: false })
  @ApiQuery({ name: 'l2', type: 'string', required: false })
  @ApiQuery({ name: 'l3', type: 'string', required: false })
  getRevDailyChart(@Req() request: Request): Promise<RevDailyChartResponse> {
    return this.revDailyService.getChart(request.query as RevDailyQuery);
  }

  @Get('branch-cluster-table')
  @ApiOperation({ summary: 'Menampilkan table Branch / Cluster' })
  @ApiQuery({ name: 'tahun', type: 'number' })
  @ApiQuery({ name: 'bulan', type: 'number' })
  @ApiQuery({ name: 'teritori', type: 'string', example: 'branch' })
  @ApiQuery({ name: 'opt', type: 'string', example: 'rev' })
  @ApiQuery({ name: 'cat', type: 'string', required: false })
  @ApiQuery({ name: 'produk', type: 'string', required: false })
  @ApiQuery({ name: 'l1', type: 'string', required: false })
  getRevDailyBranchClusterTable(@Req() request: Request): Promise<any[]> {
    return this.revDailyService.getBranchClusterTable(
      request.query as RevDailyQuery,
    );
  }

  @Get('branch-chart')
  @ApiOperation({ summary: 'Menampilkan Chart Branch' })
  @ApiQuery({ name: 'tahun', type: 'number' })
  @ApiQuery({ name: 'bulan', type: 'number' })
  @ApiQuery({ name: 'cat', type: 'string', required: false })
  @ApiQuery({ name: 'produk', type: 'string', required: false })
  @ApiQuery({ name: 'l1', type: 'string', required: false })
  @ApiQuery({ name: 'l2', type: 'string', required: false })
  @ApiQuery({ name: 'l3', type: 'string', required: false })
  getRevDailyBranchChart(
    @Req() request: Request,
  ): Promise<RevBranchChartResponse[]> {
    return this.revDailyService.getBranchChart(request.query as RevDailyQuery);
  }
}
