import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { RevDailyController } from './rev-daily.controller';
import { RevDailyService } from './rev-daily.service';

describe('RevDailyController', () => {
  let controller: RevDailyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      controllers: [RevDailyController],
      providers: [RevDailyService],
    }).compile();

    controller = module.get<RevDailyController>(RevDailyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
