import { Module } from '@nestjs/common';
import { RevDailyController } from './rev-daily.controller';
import { RevDailyService } from './rev-daily.service';

@Module({
  controllers: [RevDailyController],
  providers: [RevDailyService],
})
export class RevDailyModule {}
