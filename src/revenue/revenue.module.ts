import { Module } from '@nestjs/common';
import { RevDailyModule } from './rev-daily/rev-daily.module';
import { RevenueService } from './revenue.service';

@Module({
  imports: [RevDailyModule],
  providers: [RevenueService],
})
export class RevenueModule {}
