import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RevenueModule } from './revenue/revenue.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      name: 'conn110',
      type: 'mysql',
      host: process.env.CONN110_MYSQL_HOST,
      port: +process.env.CONN110_MYSQL_PORT,
      username: process.env.CONN110_MYSQL_USERNAME,
      password: process.env.CONN110_MYSQL_PASSWORD,
      database: process.env.CONN110_MYSQL_DATABASE,
      autoLoadEntities: true,
      synchronize: false,
    }),
    TypeOrmModule.forRoot({
      name: 'conn222',
      type: 'mysql',
      host: process.env.CONN222_MYSQL_HOST,
      port: +process.env.CONN222_MYSQL_PORT,
      username: process.env.CONN222_MYSQL_USERNAME,
      password: process.env.CONN222_MYSQL_PASSWORD,
      database: process.env.CONN222_MYSQL_DATABASE,
      autoLoadEntities: true,
      synchronize: false,
    }),
    RevenueModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
