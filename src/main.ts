import { NestFactory } from '@nestjs/core';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('salesapi');

  const AppTitle = 'SRM Kal API';
  const config = new DocumentBuilder()
    .setTitle(AppTitle)
    .setVersion('1.0')
    .addTag('revenue')
    .addTag('recharge')
    .addTag('obc')
    .addTag('wa_sms_caster')
    .build();
  const customOptions: SwaggerCustomOptions = {
    customSiteTitle: AppTitle,
  };
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/salesapi', app, document, customOptions);

  const APP_PORT = process.env.APP_PORT || 3000;

  await app.listen(APP_PORT);
  console.log(`App listening at http://localhost:${APP_PORT}`);
}
bootstrap();
